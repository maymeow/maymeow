# 안녕하세요! 👋, I'm MayMeow.

MayMeow is a skilled developer and cybersecurity enthusiast with a passion for combining technology, creativity, and security. Specializing in PHP, JavaScript, and .NET, they contribute to open-source projects. Their expertise spans software development, DevSecOps, and cryptographic solutions, where they create tools that simplify encryption.

Known for optimizing workflows and balancing cloud and self-hosted solutions, MayMeow explores new frontiers in DevSecOps and cybersecurity. Their work reflects a commitment to innovation and safeguarding the digital world, earning a reputation as both a tech expert and a creative thinker.

[![An image of @maymeow's Holopin badges, which is a link to view their full Holopin profile](https://holopin.me/maymeow)](https://holopin.io/@maymeow)

- [blog](https://blog.maymeow.com)
- [github](https://github.com/MayMeow)
- [gitlab](https://gitlab.com/MayMeow)
- [codeberg](https://codeberg.org/MayMeow)
- [mastodon](https://social.lol/@may)
- [statuslog](https://status.maymeow.lol/)
- [bluesky](https://bsky.app/profile/maymeow.com)
- [x / twitter](https://x.com/TheMayMeow)
- [signal](https://signal.me/#eu/KSpSWOtsoh-vH7fOlsd8CEMUtkQheXNauAAR16_0cbg_rAVsGxRIx5gq8ERyuJnF)
- [steam](https://steamcommunity.com/id/maysiemeow/)
- [last.fm](https://www.last.fm/user/maysiemeow)
- [spotify](https://open.spotify.com/user/8k4nk368xf4taa44ckcxatubn)
- [keyoxide](https://keyoxide.org/E4E133AD32C53C62CBB9A0E906DB2CB984185A93)
- [librepay](https://liberapay.com/maymeow/)
